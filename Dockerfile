FROM edsonalcala/pantheon:1.0.2-alpine

RUN apk add --update --no-cache bash

RUN mkdir -p /qdata

WORKDIR /qdata

EXPOSE 22001-22004

COPY . .

RUN chmod -R 777 ./

RUN echo -e '#!/bin/bash \n /qdata/start.sh ' > /usr/bin/start-nodes && \
    chmod +x /usr/bin/start-nodes

CMD ["bash"]