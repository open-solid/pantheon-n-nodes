#!/bin/bash

#### Initialize nodes #######################

#### Start node 1 #######################
nohup pantheon --config-file node_1/config_node1.toml

#### Start node 2 #######################
nohup pantheon --config-file node_2/config_node2.toml

#### Start node 3 #######################
nohup pantheon --config-file node_3/config_node3.toml

#### Start node 4 #######################
nohup pantheon --config-file node_4/config_node4.toml

echo "All nodes configured."

exit 0