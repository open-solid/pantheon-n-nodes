#!/bin/bash

privateKeys=()
addresses=(8304cB99e989EE34af465Db1CF15E369D8402870 1AFfE81865fA9b3184ddB6041feDC613054DDcC4 88f4E7D08bd9216d682196dcBd8F087854da16d1 7F4fd9Bc518F560Df6848837c657a7A9D1368bC1)

#### Copy keys from files to array ##################
for i in {1..4}
do 
  pk=`cat keys/key$i`
  echo "Key $i $pk"
  privateKeys[i-1]=$pk
done

echo '[0] Preparing keys'

#### Create main directory ##################

nnodes=${#privateKeys[@]}

#### Create directories for each node's configuration ##################

echo '[1] Configuring for '$nnodes' nodes.'

n=1
for pk in ${privateKeys[*]}
do
    qd=node_$n
    mkdir -p $qd/logs
    let n++
done

### Copy private keys to nodekey in each folder
n=1
for key in ${privateKeys[*]}
do
    qd=node_$n
    cp keys/key$n $qd/key
    let n++
done

#### Make static-nodes.json and store keys #############################

echo '[2] Creating Enodes and static-nodes.json.'

echo "[" > static-nodes.json
n=1
for key in ${privateKeys[*]}
do
    qd=node_$n

    # Generate the node's Enode and key
    enode=`cat $qd/key`
    echo "The current nodekey is '$enode'"

    pantheon --data-path=$qd public-key export --to=$qd/key.pub
    enode=`cat $qd/key.pub | sed 's/^0x//'`

    # Add the enode to static-nodes.json
    sep=`[[ $n < $nnodes ]] && echo ","`
    echo '  "enode://'$enode'@0.0.0.0:3300'$n'"'$sep >> static-nodes.json
    let n++
done
echo "]" >> static-nodes.json

#### Copy static-nodes.json to their respective folder #######################
n=1
for privkey in ${privateKeys[*]}
do
    qd=node_$n
    cp ./static-nodes.json $qd
    let n++
done

#### Create permissions and config file
echo 'Creating permissions and config file'
n=1
for key in ${privateKeys[*]}
do
    qd=node_$n

    cp templates/config_node.toml $qd
    cp templates/permissions_config.toml $qd

    # Replace node number in config_node
    sed -i '' -e "s/#NODE/$n/g" $qd/config_node.toml

    # Generate the node's Enode and key
    enode=`cat $qd/key.pub | sed 's/^0x//'`
    echo "The current node pub key is '$enode'"
    listEnodes=`cat static-nodes.json | tr -d " \t\n\r"`

    # Replace bootnodes in config_node
    sed -i '' -e "s%BOOTNODES%$listEnodes%g" $qd/config_node.toml
    sed -i '' -e "s%NODES_WHITELIST%$listEnodes%g" $qd/permissions_config.toml
    let n++
done

#### Create accounts, keys and genesis.json file #######################

echo '[3] Creating Ether accounts and genesis.json.'

cat > genesis.json <<EOF
{
  "alloc": {
EOF

n=1
for address in ${addresses[*]}
do
    qd=node_$n
    account=$address
    echo "The account is '$account'"

    # Add the account to the genesis block so it has some Ether at start-up
    sep=`[[ $n < $nnodes ]] && echo ","`
    cat >> genesis.json <<EOF
    "${account}": {
      "balance": "1000000000000000000000000000"
    }${sep}
EOF

    let n++
done

cat >> genesis.json <<EOF
  },
  "coinbase": "0x0000000000000000000000000000000000000000",
  "config": {
    "chainId": 1981,
    "constantinoplefixblock": 0,
    "ibft2": {
      "blockperiodseconds": 2,
      "epochlength": 30000,
      "requesttimeoutseconds": 10
    }
  },
  "gasLimit": "0x47b760",
  "difficulty": "0x1",
  "extraData": "0xf83ea00000000000000000000000000000000000000000000000000000000000000000d594c2ab482b506de561668e07f04547232a72897daf808400000000c0",
  "mixhash"    : "0x63746963616c2062797a616e74696e65206661756c7420746f6c6572616e6365",
  "nonce": "0x0",
  "timestamp": "0x00",
  "gasUsed": "0x0"
}
EOF

#### Remove static-nodes.json #######################

rm -f ./static-nodes.json
# remove files inside templates
# fix the cp for a mv